# product-recognition-tensorflow

# Setup
## Download and Install Docker on your machine

## Install Python along with all the other requirements needed for Tensorflow to run properly

https://www.tensorflow.org/install/install_linux

## Create a new project directory and navigate to that directory from terminal

## Run the docker inside the same project directory using this command:

```
docker run -it \
  --volume ${HOME}/Projects/product-recognition-tensorflow:/product-recognition-tensorflow \
  --workdir /product-recognition-tensorflow \
  tensorflow/tensorflow:1.1.0 bash
```

## Categorize the image files on which you want to train the models in separate directories and then add them to the `tf_files/images/` directory

## Use the following python command to train your models:

```
python -m scripts.retrain \
  --bottleneck_dir=tf_files/bottlenecks \
  --model_dir=tf_files/models/mobilenet_1.0_224\
 --summaries_dir=tf_files/training_summaries/mobilenet_1.0_224 \
 --output_graph=tf_files/retrained_graph.pb \
  --output_labels=tf_files/retrained_labels.txt \
  --architecture=mobilenet_1.0_224 \
 --image_dir=tf_files/images
```

## Optional Step: Optimize the generated files for using it inside an Android application

### Use the following command to optimize your `retrained_graph.pb`:

```
python -m tensorflow.python.tools.optimize_for_inference \
  --input=tf_files/retrained_graph.pb \
  --output=tf_files/optimized_graph.pb \
  --input_names="input" \
  --output_names="final_result"
```

### Compress your model using gzip

```
gzip -c tf_files/optimized_graph.pb > tf_files/optimized_graph.pb.gz
gzip -l tf_files/optimized_graph.pb.gz
```

## Create a new Android project in Android Studio

## Add the following dependencies to your project

```
  repositories {
     jcenter()
  }

  dependencies {
     compile 'org.tensorflow:tensorflow-android:+'
  }
```

## Move the `.pb` and `.txt` files to the assets directory of your Android project

# Usage
## Using the TensorFlow Inference Interface
The code interfacing to the TensorFlow is all contained in TensorFlowImageClassifier.java.

### Create the Interface
The first block of interest simply creates a TensorFlowInferenceInterface, which loads the named TensorFlow graph using the assetManager.

This is similar to a tf.Session (for those familiar with TensorFlow in Python).

`TensorFlowImageClassifier.java`

```
// load the model into a TensorFlowInferenceInterface.
c.inferenceInterface = new TensorFlowInferenceInterface(
    assetManager, modelFilename);
```

### Inspect the output node
This model can be retrained with different numbers of output classes. To ensure that we build an output array with the right size, we inspect the TensorFlow operations:

`TensorFlowImageClassifier.java`

```
// Get the tensorflow node
final Operation operation = c.inferenceInterface.graphOperation(outputName);

// Inspect its shape
final int numClasses = (int) operation.output(0).shape().size(1);

// Build the output array with the correct size.
c.outputs = new float[numClasses];
```

### Feed in the input
To run the network, we need to feed in our data. We use the feed method for that. To use feed, we must pass:

the name of the node to feed the data to
the data to put in that node
the shape of the data
The following lines execute the feed method.

`TensorFlowImageClassifier.java`

```
inferenceInterface.feed(
    inputName,   // The name of the node to feed.
    floatValues, // The array to feed
    1, inputSize, inputSize, 3 ); // The shape of the array
```

### Execute the calculation

Now that the inputs are in place, we can run the calculation.

Note how this run method takes an array of output names because you may want to pull more than one output. It also accepts a boolean flag to control logging.

`TensorFlowImageClassifier.java`

```
inferenceInterface.run(
        outputNames, // Names of all the nodes to calculate.
        logStats);   // Bool, enable stat logging.
```

### Fetch the output
Now that the output has been calculated we can pull it out of the model into a local variable.
The outputs array here is the one we sized by inspecting the output Operation earlier.

Call this fetch method once for each output you wish to fetch.

`TensorFlowImageClassifier.java`

```
inferenceInterface.fetch(
        outputName,  // Fetch this output.
        outputs);    // Into the prepared array.
```

# References:

https://codelabs.developers.google.com/codelabs/tensorflow-for-poets-2/

/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package org.tensorflow.demo;

public final class R {
  public static final class attr {
    /**
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     */
    public static final int cardBackgroundColor=0x7f010000;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int cardCornerRadius=0x7f010001;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int cardElevation=0x7f010002;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int cardMaxElevation=0x7f010003;
    /**
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     */
    public static final int cardPreventCornerOverlap=0x7f010004;
    /**
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     */
    public static final int cardUseCompatPadding=0x7f010005;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int contentPadding=0x7f010006;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int contentPaddingBottom=0x7f010007;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int contentPaddingLeft=0x7f010008;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int contentPaddingRight=0x7f010009;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int contentPaddingTop=0x7f01000a;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int metaButtonBarButtonStyle=0x7f01000b;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int metaButtonBarStyle=0x7f01000c;
  }
  public static final class color {
    public static final int black=0x7f020000;
    public static final int cardview_dark_background=0x7f020001;
    public static final int cardview_light_background=0x7f020002;
    public static final int cardview_shadow_end_color=0x7f020003;
    public static final int cardview_shadow_start_color=0x7f020004;
    public static final int control_background=0x7f020005;
    public static final int transparent_black=0x7f020006;
    public static final int white=0x7f020007;
  }
  public static final class dimen {
    public static final int cardview_compat_inset_shadow=0x7f030000;
    public static final int cardview_default_elevation=0x7f030001;
    public static final int cardview_default_radius=0x7f030002;
    public static final int horizontal_page_margin=0x7f030003;
    public static final int margin_huge=0x7f030004;
    public static final int margin_large=0x7f030005;
    public static final int margin_medium=0x7f030006;
    public static final int margin_small=0x7f030007;
    public static final int margin_tiny=0x7f030008;
    public static final int vertical_page_margin=0x7f030009;
  }
  public static final class drawable {
    public static final int butter=0x7f040000;
    public static final int foundation=0x7f040001;
    public static final int ic_action_info=0x7f040002;
    public static final int ic_launcher=0x7f040003;
    public static final int quench=0x7f040004;
    public static final int remover=0x7f040005;
    public static final int sunkissed=0x7f040006;
    public static final int tile=0x7f040007;
  }
  public static final class id {
    public static final int container=0x7f050000;
    public static final int debug_overlay=0x7f050001;
    public static final int image_product=0x7f050002;
    public static final int results=0x7f050003;
    public static final int text_name=0x7f050004;
    public static final int texture=0x7f050005;
    public static final int titlebar=0x7f050006;
  }
  public static final class layout {
    public static final int activity_camera=0x7f060000;
    public static final int camera_connection_fragment=0x7f060001;
    public static final int item_product=0x7f060002;
  }
  public static final class string {
    public static final int activity_name_classification=0x7f070000;
    public static final int activity_name_detection=0x7f070001;
    public static final int activity_name_stylize=0x7f070002;
    public static final int app_name=0x7f070003;
    public static final int camera_error=0x7f070004;
    public static final int description_info=0x7f070005;
    public static final int request_permission=0x7f070006;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f080000;
    public static final int AppTheme=0x7f080001;
    public static final int Base_CardView=0x7f080002;
    public static final int CardView=0x7f080003;
    public static final int CardView_Dark=0x7f080004;
    public static final int CardView_Light=0x7f080005;
    public static final int FullscreenActionBarStyle=0x7f080006;
    public static final int FullscreenTheme=0x7f080007;
    public static final int MaterialTheme=0x7f080008;
    public static final int Theme_Base=0x7f080009;
    public static final int Theme_Sample=0x7f08000a;
    public static final int Widget=0x7f08000b;
    public static final int Widget_SampleMessage=0x7f08000c;
    public static final int Widget_SampleMessageTile=0x7f08000d;
  }
  public static final class styleable {
    /**
     * Attributes that can be used with a ButtonBarContainerTheme.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #ButtonBarContainerTheme_metaButtonBarButtonStyle org.tensorflow.demo:metaButtonBarButtonStyle}</code></td><td></td></tr>
     * <tr><td><code>{@link #ButtonBarContainerTheme_metaButtonBarStyle org.tensorflow.demo:metaButtonBarStyle}</code></td><td></td></tr>
     * </table>
     * @see #ButtonBarContainerTheme_metaButtonBarButtonStyle
     * @see #ButtonBarContainerTheme_metaButtonBarStyle
     */
    public static final int[] ButtonBarContainerTheme={
        0x7f01000b, 0x7f01000c
      };
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#metaButtonBarButtonStyle}
     * attribute's value can be found in the {@link #ButtonBarContainerTheme} array.
     *
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     *
     * @attr name org.tensorflow.demo:metaButtonBarButtonStyle
     */
    public static final int ButtonBarContainerTheme_metaButtonBarButtonStyle=0;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#metaButtonBarStyle}
     * attribute's value can be found in the {@link #ButtonBarContainerTheme} array.
     *
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     *
     * @attr name org.tensorflow.demo:metaButtonBarStyle
     */
    public static final int ButtonBarContainerTheme_metaButtonBarStyle=1;
    /**
     * Attributes that can be used with a CardView.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #CardView_android_minWidth android:minWidth}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_android_minHeight android:minHeight}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardBackgroundColor org.tensorflow.demo:cardBackgroundColor}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardCornerRadius org.tensorflow.demo:cardCornerRadius}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardElevation org.tensorflow.demo:cardElevation}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardMaxElevation org.tensorflow.demo:cardMaxElevation}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardPreventCornerOverlap org.tensorflow.demo:cardPreventCornerOverlap}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_cardUseCompatPadding org.tensorflow.demo:cardUseCompatPadding}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_contentPadding org.tensorflow.demo:contentPadding}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingBottom org.tensorflow.demo:contentPaddingBottom}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingLeft org.tensorflow.demo:contentPaddingLeft}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingRight org.tensorflow.demo:contentPaddingRight}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingTop org.tensorflow.demo:contentPaddingTop}</code></td><td></td></tr>
     * </table>
     * @see #CardView_android_minWidth
     * @see #CardView_android_minHeight
     * @see #CardView_cardBackgroundColor
     * @see #CardView_cardCornerRadius
     * @see #CardView_cardElevation
     * @see #CardView_cardMaxElevation
     * @see #CardView_cardPreventCornerOverlap
     * @see #CardView_cardUseCompatPadding
     * @see #CardView_contentPadding
     * @see #CardView_contentPaddingBottom
     * @see #CardView_contentPaddingLeft
     * @see #CardView_contentPaddingRight
     * @see #CardView_contentPaddingTop
     */
    public static final int[] CardView={
        0x0101013f, 0x01010140, 0x7f010000, 0x7f010001, 
        0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 
        0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009, 
        0x7f01000a
      };
    /**
     * <p>This symbol is the offset where the {@link android.R.attr#minHeight}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android:minHeight
     */
    public static final int CardView_android_minHeight=1;
    /**
     * <p>This symbol is the offset where the {@link android.R.attr#minWidth}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android:minWidth
     */
    public static final int CardView_android_minWidth=0;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardBackgroundColor}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     *
     * @attr name org.tensorflow.demo:cardBackgroundColor
     */
    public static final int CardView_cardBackgroundColor=2;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardCornerRadius}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:cardCornerRadius
     */
    public static final int CardView_cardCornerRadius=3;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardElevation}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:cardElevation
     */
    public static final int CardView_cardElevation=4;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardMaxElevation}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:cardMaxElevation
     */
    public static final int CardView_cardMaxElevation=5;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardPreventCornerOverlap}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     *
     * @attr name org.tensorflow.demo:cardPreventCornerOverlap
     */
    public static final int CardView_cardPreventCornerOverlap=6;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#cardUseCompatPadding}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     *
     * @attr name org.tensorflow.demo:cardUseCompatPadding
     */
    public static final int CardView_cardUseCompatPadding=7;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#contentPadding}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:contentPadding
     */
    public static final int CardView_contentPadding=8;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#contentPaddingBottom}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:contentPaddingBottom
     */
    public static final int CardView_contentPaddingBottom=9;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#contentPaddingLeft}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:contentPaddingLeft
     */
    public static final int CardView_contentPaddingLeft=10;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#contentPaddingRight}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:contentPaddingRight
     */
    public static final int CardView_contentPaddingRight=11;
    /**
     * <p>This symbol is the offset where the {@link org.tensorflow.demo.R.attr#contentPaddingTop}
     * attribute's value can be found in the {@link #CardView} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name org.tensorflow.demo:contentPaddingTop
     */
    public static final int CardView_contentPaddingTop=12;
  }
}
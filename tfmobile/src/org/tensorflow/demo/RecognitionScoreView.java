/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package org.tensorflow.demo;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import org.tensorflow.demo.Classifier.Recognition;

import java.util.List;

public class RecognitionScoreView extends View implements ResultsView {
    private static final float TEXT_SIZE_DIP = 15;
    private List<Recognition> results;
    private final float textSizePx;
    private final Paint fgPaint;
    private final Paint bgPaint;
    private final int height, width;
    private final int IMAGE_SIZE, PADDING;

    public RecognitionScoreView(final Context context, final AttributeSet set) {
        super(context, set);

        textSizePx =
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        fgPaint = new Paint();
        fgPaint.setColor(getResources().getColor(R.color.white));
        fgPaint.setTextSize(textSizePx);

        bgPaint = new Paint();
        bgPaint.setColor(getResources().getColor(R.color.transparent_black));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        IMAGE_SIZE = Math.round(convertDpToPixel(100, context));
        PADDING = Math.round(convertDpToPixel(20, context));
    }

    @Override
    public void setResults(final List<Recognition> results) {
        this.results = results;
        postInvalidate();
    }

    @Override
    public void onDraw(final Canvas canvas) {
        int x = width / 2;
        int y = (int) (fgPaint.getTextSize() * 1.5f);

        canvas.drawPaint(bgPaint);
        Drawable d = null;


        if (results != null && results.size() > 0) {
            Recognition recog = results.get(0);
            Log.i("Result", recog.getTitle() + ": " + recog.getConfidence());
            if (recog.getConfidence() > 0.95) {

                if (recog.getTitle().contains("butter")) {
                    d = getResources().getDrawable(R.drawable.butter);
                } else if (recog.getTitle().contains("foundation")) {
                    d = getResources().getDrawable(R.drawable.foundation);
                } else if (recog.getTitle().contains("quench")) {
                    d = getResources().getDrawable(R.drawable.quench);
                } else if (recog.getTitle().contains("remover")) {
                    d = getResources().getDrawable(R.drawable.remover);
                } else if (recog.getTitle().contains("sunkissed")) {
                    d = getResources().getDrawable(R.drawable.sunkissed);
                }
                d.setBounds(PADDING, 0, IMAGE_SIZE, IMAGE_SIZE);
                d.draw(canvas);

                x = IMAGE_SIZE + PADDING / 2;
                //canvas.drawText(recog.getTitle() + "\nConfidence:" + recog.getConfidence(), x, y, fgPaint);
                canvas.drawText(recog.getTitle(), x, y, fgPaint);
                y += fgPaint.getTextSize() * 1.5f;
            }

        }
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

}

package org.tensorflow.demo;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by rahul on 11/4/18.
 */

public class ProductCard extends RelativeLayout {
    private TextView name;
    //private TextView description;
    private ImageView icon;

    public ProductCard(Context context) {
        super(context);
        init();
    }

    public ProductCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProductCard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_product, this);
        this.name = (TextView) findViewById(R.id.text_name);
        //this.description = (TextView) findViewById(R.id.description);
        this.icon = (ImageView) findViewById(R.id.image_product);
    }

    public void setProduct(List<Classifier.Recognition> results) {

        Classifier.Recognition recog = results.get(0);
        Drawable d = null;
        if (recog.getTitle().contains("butter")) {
            d = getResources().getDrawable(R.drawable.butter);
        } else if (recog.getTitle().contains("foundation")) {
            d = getResources().getDrawable(R.drawable.foundation);
        } else if (recog.getTitle().contains("quench")) {
            d = getResources().getDrawable(R.drawable.quench);
        } else if (recog.getTitle().contains("remover")) {
            d = getResources().getDrawable(R.drawable.remover);
        } else if (recog.getTitle().contains("sunkissed")) {
            d = getResources().getDrawable(R.drawable.sunkissed);
        }
        icon.setImageDrawable(d);
        name.setText(recog.getTitle());
    }
}
